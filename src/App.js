import './App.css';
import InputComponent from './components/inputComponents/InputComponent';
import Outputcomponent from './components/inputComponents/OutputComponent';
import LoginField from './components/loginComponents/LoginField'
import { BrowserRouter, Switch, Route } from "react-router-dom"
import ProfileInfoComponent from './components/profileComponents/ProfileInfoComponent';




function App() {
  return (
    <BrowserRouter>
      <div>
        <Switch>
          <Route exact path="/" component={LoginField} />
          <Route path="/translate">
            <InputComponent />
            <Outputcomponent />
          </Route>
          <Route path="/profile" >
            <ProfileInfoComponent />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>

  );
}

export default App;
