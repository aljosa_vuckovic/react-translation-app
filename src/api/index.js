
const BASE_URL="https://fast-castle-43673.herokuapp.com/"

export const getUserInfo = async ()=>{

    const response= await fetch(`${BASE_URL}users`)
    const data = await response.json()

    return data

}


export const setUser = async (name)=>{

    const response= await fetch(`${BASE_URL}users`,{
        method:'POST',
        headers:{
            'Content-Type': 'application/json'
        },
        body:JSON.stringify({
            name:name,
            words:[]
        })
    })
    const data = await response.json()

    return data

}


export const setUserObject = async (user)=>{

    const response= await fetch(`${BASE_URL}users/${user.id}`,{
        method:'PUT',
        headers:{
            'Content-Type': 'application/json'
        },
        body:JSON.stringify({
            id:user.id,
            name:user.name,
            words:user.words
        })
    })
    const data = await response.json()

    return data

}