import { useEffect, useState } from "react"
import { getUserInfo, setUser } from "../../api"
import { useHistory } from "react-router"
import { Button, Typography, Grid } from "@material-ui/core"
import { ThemeProvider, makeStyles } from '@material-ui/core/styles';
import theme from '../../theme'


const useStyles = makeStyles({
    textStyle: {
        color: "red"
    },
    buttonStyle: {
        color: "purple"
    }
})

function LoginField() {

    const history = useHistory()
    const [userName, setUserName] = useState('')
    const [users, setUsers] = useState([])



    /**
     * The hook fetches all the currently registered users from the json file and inserts them as state of the component.
     * The if statements evaluates the local storage for currently signed users in which case the user (if existing) is redirected to the second page (input/output components)
     * else the 
     */
    useEffect(() => {
        (async () => {
            const result = await getUserInfo()
            console.log("the result is", result);
            setUsers(result)
        })()

        if (localStorage.getItem("user") !== null) {
            history.push("/translate")
        } else {
            localStorage.clear()
        }
    }, [])

    /**
     * @param {onChange} event
     * The method dynamicly changes the user name in localstorage that is later user to start a session once the user chooses to
     * The event is coming for the input field  
     */
    const loginToApp = event => {
        setUserName(event.target.value)
        localStorage.setItem("user", event.target.value)
    }
    /**
     * The method attemps to start a user session
     * The outer if statement handles the condition of entering a user name in order to login
     * The inner if statement evaluates the json file for the user name that has been inserted, 
     * depending on if the user name is already existing in the file it is eather imported or fetched from the file  
     */
    const StartSession = () => {
        if (userName === "") {
            alert("Please enter a user name in order to login")
        } else {
            let array = []
            let index = 0;
            users.forEach(element => {
                array.push(element.name)
            });
            index++;
            if (array.includes(userName)) {
                console.log(array);
                console.log("The user is already registered");
                localStorage.setItem("user", userName)
                history.push("/translate")
            } else {
                console.log(array);
                console.log("The user is not registered");
                (async () => {
                    const result = await setUser(userName)
                    console.log("the result is", result);
                    setUsers(result)
                    setUserName(userName)
                    localStorage.setItem("user", userName)
                    history.push("/translate")
                })()
            }
            
        }


    }


    const classes = useStyles();
    return (
        < Grid container spacing={2} direction="column">  
            <ThemeProvider theme={theme}>
            <Grid container item xs={12} color="Red"></Grid>
            <Grid container item xs={12} > </Grid>
                <Grid item xs={12} container>
                    <Grid item xs={3}> <form>
                        <Typography className={classes.textStyle} variant="h6" color="primary" > <label for="userName" >User Name</label></Typography>
                        <input id="userName" type="text" onChange={loginToApp}></input>
                    </form>
                    </Grid >
                    <Grid item xs={6} >
                        <Typography className={classes.textStyle} variant="h3" color="primary" align="center"  >
                            <label>Super-Extra-Mega-Ultra-Sign Language Translator +</label>
                        </Typography>
                    </Grid>
                    <Grid item xs={3} align="right">
                        <Button className={classes.buttonStyle} color="primary" type="Reset" variant="outlined" onClick={StartSession} >Login</Button>
                    </Grid>
                </Grid>
            </ThemeProvider>
        </Grid>
    )
}
export default LoginField