import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { getUserInfo, setUserObject } from "../../api"

function ProfileInfoComponent() {
    
    const history = useHistory()
    const [user, setUser] = useState(null)

    /**
     * The function fetches all the users currently stored in the json file
     * The user name currently stored in the local storage of the component is then used as matching to get the matching user object with the same name.
     * The user object's word array is then looped throu where every word s marked as deleted
     * The object with the updated words array is then logged into the json file again and is set as a state of the component
     */
    
    function deleteHistory() {
         
        const fetchPost = async () => {
            let userName = localStorage.getItem("user")
            const result = await getUserInfo()
            console.log("the result is", result)
            const filteredUser = result.find(u => u.name === userName)
            console.log(filteredUser)
            for (let i = 0; i < filteredUser.words.length; i++) {
                filteredUser.words[i] = "deleted"
            }
            await setUserObject(filteredUser)
            console.log("le data is: ", filteredUser)
            setUser(filteredUser)
        }
        fetchPost()
    }

    /**
     * The hook fetches all the users currently stored in the json file
     * The user name currently stored as a state of the component is then used as matching to get the matching user object with the same name. The object is later set as state of the component and used as current user.
     * The if statement checks local storage for a stored value. The reason behind the condition is that users who are not logged in should be able to route to this component. 
     */
    useEffect(() => {
        const fetchPost = async () => {
            let userName = localStorage.getItem("user")
            const result = await getUserInfo()
            console.log("the result is", result)
            const filteredUser = result.find(u => u.name === userName)
            console.log(filteredUser)
            console.log("le data is: ", filteredUser)
            setUser(filteredUser)
        }
        fetchPost()
        console.log(user);
        if (localStorage.getItem("user") === null) {
            history.push("/")
        }
    }, [])

    const logOut = () => {
        localStorage.clear()
        history.push("/")
    }
    const goToHomePage = () => {
        history.push("/")
    }
    return (
        <div>
            <p>
                Hello  {user && user.name} <br></br>
                Your translation history is: <br></br>
            </p>
            {user && user.words.map((word) => {
                if (word != "deleted") {
                    return <h1>{word}</h1>
                }
            }
            )}
            <button type="reset" onClick={deleteHistory} >Delete you translation history</button>
            <p>
                <button onClick={goToHomePage}>Home</button>
            </p>
            <p>
                <button onClick={logOut} >Logg out</button>
            </p>
        </div>
    )
}

export default ProfileInfoComponent