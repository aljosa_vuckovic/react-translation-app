import { element } from "prop-types";
import { useEffect, useState } from "react";
import { getUserInfo, setUser, setUserObject } from "../../api"
import { Link, useHistory } from 'react-router-dom'
function Outputcomponent() {

    let signs = new Map();
    signs.set("a", <img src='./images/a.png' alt="a.png" />)
    signs.set("A", <img src='./images/a.png' alt="a.png" />)

    signs.set("b", <img src='./images/b.png' alt="b.png" />)
    signs.set("B", <img src='./images/b.png' alt="b.png" />)

    signs.set("c", <img src='./images/c.png' alt="c.png" />)
    signs.set("C", <img src='./images/c.png' alt="c.png" />)

    signs.set("d", <img src='./images/d.png' alt="d.png" />)
    signs.set("D", <img src='./images/d.png' alt="d.png" />)

    signs.set("e", <img src='./images/e.png' alt="e.png" />)
    signs.set("E", <img src='./images/e.png' alt="e.png" />)

    signs.set("f", <img src='./images/f.png' alt="f.png" />)
    signs.set("F", <img src='./images/f.png' alt="f.png" />)

    signs.set("g", <img src='./images/g.png' alt="g.png" />)
    signs.set("G", <img src='./images/g.png' alt="g.png" />)

    signs.set("h", <img src='./images/h.png' alt="h.png" />)
    signs.set("H", <img src='./images/h.png' alt="h.png" />)

    signs.set("i", <img src='./images/i.png' alt="i.png" />)
    signs.set("I", <img src='./images/i.png' alt="i.png" />)

    signs.set("J", <img src='./images/j.png' alt="j.png" />)
    signs.set("j", <img src='./images/b.png' alt="j.png" />)

    signs.set("k", <img src='./images/k.png' alt="k.png" />)
    signs.set("K", <img src='./images/k.png' alt="k.png" />)

    signs.set("l", <img src='./images/l.png' alt="l.png" />)
    signs.set("L", <img src='./images/l.png' alt="l.png" />)

    signs.set("m", <img src='./images/m.png' alt="m.png" />)
    signs.set("M", <img src='./images/m.png' alt="m.png" />)

    signs.set("n", <img src='./images/n.png' alt="n.png" />)
    signs.set("N", <img src='./images/n.png' alt="n.png" />)

    signs.set("o", <img src='./images/o.png' alt="o.png" />)
    signs.set("O", <img src='./images/o.png' alt="o.png" />)

    signs.set("p", <img src='./images/p.png' alt="p.png" />)
    signs.set("P", <img src='./images/p.png' alt="p.png" />)

    signs.set("q", <img src='./images/q.png' alt="q.png" />)
    signs.set("Q", <img src='./images/q.png' alt="q.png" />)

    signs.set("r", <img src='./images/r.png' alt="r.png" />)
    signs.set("R", <img src='./images/r.png' alt="r.png" />)

    signs.set("s", <img src='./images/s.png' alt="s.png" />)
    signs.set("S", <img src='./images/s.png' alt="s.png" />)

    signs.set("t", <img src='./images/t.png' alt="t.png" />)
    signs.set("T", <img src='./images/t.png' alt="t.png" />)

    signs.set("u", <img src='./images/u.png' alt="u.png" />)
    signs.set("U", <img src='./images/u.png' alt="u.png" />)

    signs.set("v", <img src='./images/v.png' alt="v.png" />)
    signs.set("V", <img src='./images/v.png' alt="v.png" />)

    signs.set("w", <img src='./images/w.png' alt="w.png" />)
    signs.set("W", <img src='./images/w.png' alt="w.png" />)

    signs.set("x", <img src='./images/x.png' alt="x.png" />)
    signs.set("X", <img src='./images/x.png' alt="x.png" />)

    signs.set("y", <img src='./images/y.png' alt="y.png" />)
    signs.set("Y", <img src='./images/y.png' alt="y.png" />)

    signs.set("z", <img src='./images/z.png' alt="z.png" />)
    signs.set("Z", <img src='./images/z.png' alt="z.png" />)

    const history = useHistory()


    const [word, setWord] = useState('')
    let [letter, setletter] = useState('')
    const [users, setUsers] = useState([])
    const [userName, setUserName] = useState(localStorage.getItem("user"))
    const [currentUser, setCurrentUser] = useState({ words: [] })

    let array = []
    if (word != null) {
        array = word.split("")
    }

    /**
     * The word that is to be translated is stored in local storage.
     * In the function the word is fetched from local storage and set a state of the component
     * The word is also pushed in to the array which is a part of the user object
     * The user object is set as the state of the user and is stored in the json file    
     */
    function translateButton() {
        const foo = localStorage.getItem("wordToTranslate")
        console.log(foo)
        setWord(foo)
        console.log("the stored word is: ", word);
        currentUser.words.push(foo)
        setCurrentUser(currentUser)
        setUserObject(currentUser)
    }

    

    /**
     * The hook fetches all the users currently stored in the json file
     * The user name currently stored as a state of the component is then used as matching to get the matching user object with the same name. The ibject is later set and used as current user.
     * The if statement checks local storage for a stored value. The reason behind the condition is that users who are not logged in should be able to route to this component. 
     */
    useEffect(() => {
        (async () => {
            const result = await getUserInfo()
            console.log("the result is", result)
            const filteredUser = result.find(u => u.name === userName)
            console.log("LS: ", userName)
            console.log(result)
            console.log(filteredUser)
            setCurrentUser(filteredUser)        
        })()
        if (localStorage.getItem("user")===null) {
            history.push("/")
        }
        
    }, [])

    return (
        /**
         * The mapping function checks the current letter fo the word array for the matching word in the word map upon whcih the hand sing of the letter is displayed, or a "_" is displayed or simply the letter is displayed.
         */
        <div>
            <button type="Reset" onClick={translateButton}>Translate</button>
            <h1>{array.map((letter) => {
                if (signs.has(letter)) {
                    return signs.get(letter)
                } else if (letter = " ") {
                    return "_"
                }
                else {
                    return letter
                }
            })}
            </h1>
            <p>
                <Link to="/profile">
                    <button>Profile</button>
                </Link>
            </p>

        </div>


    )

}

export default Outputcomponent