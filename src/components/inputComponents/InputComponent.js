import {useState } from "react";
import TextField from "@material-ui/core/TextField";
import {Grid } from "@material-ui/core"


function InputComponent() {

  const [word, setWord] = useState("")
  
  return (

/**
 * The jsx code awaits a input from the use which is stored in the local storage and as state in the component
 */
    <Grid container pacing={2} item xs={12} direction="column">
      <Grid container >
        <Grid item xs={4} align="right">
          <h2>Type what you want to translate to in sign language</h2>
        </Grid>
        <Grid item xs={4} align="center" > <TextField
          value={word}
          label="Enter here"
          onChange={(e) => {
            setWord(e.target.value);
            localStorage.setItem("wordToTranslate", e.target.value)
          }}
        /></Grid>
        <Grid item xs={4} align="left">
          <h3>Your Enter Value is: {word} </h3>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default InputComponent;